// Iteración #3: Mix Fors
// Dado el siguiente javascript usa forof y forin para saber cuantas veces ha sido cada sonido 
// agregado por los usuarios a favorito. Para ello recorre la lista de usuarios y usa forin 
// para recoger el nombre de los sonidos que el usuario tenga como favoritos.
// Una vez accedas a ellos piensa en la mejor forma de hacer un conteo de cada vez que ese sonido 
// se repita como favorito en cada usuario.

const users = [
    {name: 'Manolo el del bombo',
        favoritesSounds: {
            waves: {format: 'mp3', volume: 50},
            rain: {format: 'ogg', volume: 60},
            firecamp: {format: 'mp3', volume: 80},
        }
    },
    {name: 'Mortadelo',
        favoritesSounds: {
            waves: {format: 'mp3', volume: 30},
            shower: {format: 'ogg', volume: 55},
            train: {format: 'mp3', volume: 60},
        }
    },
    {name: 'Super Lopez',
        favoritesSounds: {
            shower: {format: 'mp3', volume: 50},
            train: {format: 'ogg', volume: 60},
            firecamp: {format: 'mp3', volume: 80},
        }
    },
    {name: 'El culebra',
        favoritesSounds: {
            waves: {format: 'mp3', volume: 67},
            wind: {format: 'ogg', volume: 35},
            firecamp: {format: 'mp3', volume: 60},
        }
    },
]

let wavesFavourite = 0;
let rainFavourite= 0;
let firecampFavourite = 0;
let showerFavourite = 0;
let trainFavourite = 0;
let windFavourite = 0;


for ( datos of users) {
    for ( sound in datos.favoritesSounds){
        if (sound == 'waves' ){
            wavesFavourite++;
        }
        else if (sound == 'rain'){
            rainFavourite++;
        }

       
        else if (sound == 'firecamp'){
            firecampFavourite++;
        }
        else if (sound == 'shower'){
            showerFavourite++;
        }  

        else if (sound == 'train'){
            trainFavourite++;
        } 

        else {
            windFavourite++;
        }
    }

}

console.log( 'waves =' + wavesFavourite + '\n' , 'rain =' + rainFavourite + '\n' , 'firecamp =' +firecampFavourite + '\n', 'shower =' +showerFavourite + '\n' , 'train =' +trainFavourite+ '\n', 'wind ='+windFavourite + '\n'  );